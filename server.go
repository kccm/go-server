package server

import (
	"net/http"

	"github.com/gorilla/context"
	"github.com/julienschmidt/httprouter"

	log "gitlab.com/kccm/go-logger"
	info "gitlab.com/kccm/go-server/pkg/handlers/info"
	prometheus "gitlab.com/kccm/go-server/pkg/handlers/prometheus"
	swagger "gitlab.com/kccm/go-server/pkg/handlers/swagger"
	middleware "gitlab.com/kccm/go-server/pkg/middleware"
)

// Start starts the HTTP and/or HTTPS listener
func Start(host string, port string, router *httprouter.Router, middlewares ...func(http.Handler) http.Handler) {
	log.Info("SYSTEM", "Server started", log.Fields{"server_address": host + ":" + port})
	// Start the HTTP listener
	log.Fatal("SYSTEM", http.ListenAndServe(host+":"+port, setMiddleware(router, middlewares...)), "Server failed")
}

func setMiddleware(h http.Handler, middlewares ...func(http.Handler) http.Handler) http.Handler {
	// Extra Middlewares
	if middlewares != nil {
		for _, mdl := range middlewares {
			h = mdl(h)
		}
	}
	// Api log
	h = middleware.ApiLog(h)
	// Cros ref
	h = middleware.Cors(h)
	// Clear handler for Gorilla Context
	h = context.ClearHandler(h)
	return h
}

// Handlers
func SetInfo(r *httprouter.Router, verFilePath string) {
	info.Set(r, verFilePath)
}

func SetSwagger(r *httprouter.Router, swaggeruiPath string) {
	swagger.Set(r, swaggeruiPath)
}

func SetPrometheus(r *httprouter.Router) {
	prometheus.Set(r)
}
