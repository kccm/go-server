package middleware

import (
	"net/http"

	log "gitlab.com/kccm/go-logger"
	ipLookUp "gitlab.com/kccm/go-server/pkg/ipLookUp"
)

// Handler will allow cross-origin HTTP requests
func Cors(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// Set allow origin to match origin of our request or fall back to *
		if originL := r.Header.Get("Local-Origin"); originL != "" {
			w.Header().Set("Access-Control-Allow-Origin", originL) // local debug
		} else if origin := r.Header.Get("Origin"); origin != "" {
			w.Header().Set("Access-Control-Allow-Origin", origin)
		} else {
			w.Header().Set("Access-Control-Allow-Origin", "*")
		}
		// Set other headers
		headers := "Authorization,Accept,Accept-Version,Content-Length,Content-MD5,Content-Type,Content-Disposition,Date,Cache-Control,Origin,Local-Origin,X-Api-Version,api-version,content-length,content-md5,content-type,date,request-id,response-time,accept,accept-version,origin,x-api-version,x-request-id,x-requested-with"
		w.Header().Set("Access-Control-Allow-Headers", headers)
		w.Header().Set("Access-Control-Expose-Headers", headers)

		methods := "POST, GET, OPTIONS, PUT, DELETE, HEAD, PATCH"
		w.Header().Set("Access-Control-Allow-Methods", methods)

		w.Header().Set("Access-Control-Allow-Credentials", "true")

		// If this was preflight options request let's write empty ok response and return
		if r.Method == "OPTIONS" {
			w.WriteHeader(http.StatusOK)
			w.Write(nil)
			return
		}
		next.ServeHTTP(w, r)
	})
}

// Handler will log the HTTP requests
func ApiLog(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if r.URL.Path != "/info" {
			log.Info("SYSTEM", "api usage log", log.Fields{
				"ip_address": ipLookUp.GetIPAdress(r),
				"method":     r.Method,
				"route":      r.URL.Path,
			})
		}
		next.ServeHTTP(w, r)
	})
}
