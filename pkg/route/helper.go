package route

import (
	"net/http"

	"github.com/gorilla/context"
	"github.com/julienschmidt/httprouter"
)

// Get is a shortcut for router.Handle("GET", path, handle)
func Get(path string, fn http.HandlerFunc, routers ...*httprouter.Router) {
	if routers != nil {
		Router = routers[0]
	}
	Router.GET(path, handlerFunc(fn))
}

// Post is a shortcut for router.Handle("POST", path, handle)
func Post(path string, fn http.HandlerFunc, routers ...*httprouter.Router) {
	if routers != nil {
		Router = routers[0]
	}
	Router.POST(path, handlerFunc(fn))
}

// Put is a shortcut for router.Handle("PUT", path, handle)
func Put(path string, fn http.HandlerFunc, routers ...*httprouter.Router) {
	if routers != nil {
		Router = routers[0]
	}
	Router.PUT(path, handlerFunc(fn))
}

// Patch is a shortcut for router.Handle("PATCH", path, handle)
func Patch(path string, fn http.HandlerFunc, routers ...*httprouter.Router) {
	if routers != nil {
		Router = routers[0]
	}
	Router.PATCH(path, handlerFunc(fn))
}

// Delete is a shortcut for router.Handle("DELETE", path, handle)
func Delete(path string, fn http.HandlerFunc, routers ...*httprouter.Router) {
	if routers != nil {
		Router = routers[0]
	}
	Router.DELETE(path, handlerFunc(fn))
}

// Static is a shortcut for router.Handle("Static", path, handle)
func Static(path string, folder string, routers ...*httprouter.Router) {
	if routers != nil {
		Router = routers[0]
	}
	Router.ServeFiles(path, http.Dir(folder))
}

// GetHandler is a shortcut for router.Handle("GET", path, handle)
func Gethandler(path string, fn http.Handler, routers ...*httprouter.Router) {
	if routers != nil {
		Router = routers[0]
	}
	Router.GET(path, handler(fn))
}

// Head is a shortcut for router.Handle("HEAD", path, handle)
func Head(path string, fn http.HandlerFunc, routers ...*httprouter.Router) {
	if routers != nil {
		Router = routers[0]
	}
	Router.HEAD(path, handlerFunc(fn))
}

// Options is a shortcut for router.Handle("OPTIONS", path, handle)
func Options(path string, fn http.HandlerFunc, routers ...*httprouter.Router) {
	if routers != nil {
		Router = routers[0]
	}
	Router.OPTIONS(path, handlerFunc(fn))
}

// PostHandler is a shortcut for router.Handle("POST", path, handle)
func Posthandler(path string, fn http.Handler, routers ...*httprouter.Router) {
	if routers != nil {
		Router = routers[0]
	}
	Router.POST(path, handler(fn))
}

// HandlerFunc accepts the name of a function so you don't have to wrap it with http.HandlerFunc
// Example: r.GET("/", httprouterwrapper.handlerFunc(controller.Index))
// Source: http://nicolasmerouze.com/guide-routers-golang/
func handlerFunc(h http.HandlerFunc) httprouter.Handle {
	return func(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
		context.Set(r, "params", p)
		h.ServeHTTP(w, r)
	}
}

// Handler accepts a handler to make it compatible with http.HandlerFunc
// Example: r.GET("/", httprouterwrapper.handler(http.handlerFunc(controller.Index)))
// Source: http://nicolasmerouze.com/guide-routers-golang/
func handler(h http.Handler) httprouter.Handle {
	return func(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
		context.Set(r, "params", p)
		h.ServeHTTP(w, r)
	}
}
