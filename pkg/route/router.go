package route

import (
	"io/ioutil"
	"net/http"
	"strconv"

	"github.com/gorilla/context"
	"github.com/julienschmidt/httprouter"
	//"github.com/justinas/alice"
)

var (
	Router *httprouter.Router
)

func init() {
	Router = httprouter.New()
}

func New() *httprouter.Router {
	Router = httprouter.New()
	return Router
}

// Param returns the URL parameters
func Param(r *http.Request, key string) string {
	params := context.Get(r, "params").(httprouter.Params)
	return params.ByName(key)
}

func Body(r *http.Request) ([]byte, error) {
	return ioutil.ReadAll(r.Body)
}

func Query(r *http.Request, key string) string {
	return r.URL.Query().Get(key)
}

func QueryInt(r *http.Request, key string, ranges ...int) int {
	i := 0
	if ranges != nil && len(ranges) > 2 {
		i = ranges[2] // Default
	}
	if s, err := strconv.ParseInt(r.URL.Query().Get(key), 10, 64); err == nil {
		i = int(s)
	}
	if ranges != nil {
		if i < ranges[0] {
			i = ranges[0] // Minimum
		}
		if len(ranges) > 1 && i > ranges[1] {
			i = ranges[1] // Maximum
		}
	}
	return i
}

func QueryBool(r *http.Request, key string) bool {
	if r.URL.Query().Get(key) == "true" {
		return true
	}
	return false
}

// // Chain returns handle with chaining using Alice
// func Chain(fn http.HandlerFunc, c ...alice.Constructor) httprouter.Handle {
// 	return Handler(alice.New(c...).ThenFunc(fn))
// }
