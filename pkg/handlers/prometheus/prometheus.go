package prometheus

import (
	"net/http"

	"github.com/gorilla/context"
	"github.com/julienschmidt/httprouter"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

// swagger:operation GET /prometheus Monitoring prometheus
// Prometheus monitoring system.
// library - github.com/prometheus/client_golang/prometheus/promhttp.
// ---
// responses:
//   '200':
func Set(r *httprouter.Router) {
	r.GET("/prometheus", handler(promhttp.Handler()))
}

func handler(h http.Handler) httprouter.Handle {
	return func(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
		context.Set(r, "params", p)
		h.ServeHTTP(w, r)
	}
}
