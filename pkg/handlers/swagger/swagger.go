package swagger

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"os"
	"path"
	"strconv"

	"github.com/gorilla/context"
	"github.com/julienschmidt/httprouter"

	log "gitlab.com/kccm/go-logger"
)

var (
	swaggerJson = &swaggerErrorMsg{}
)

type swaggerErrorMsg struct {
	Status        string `json:"status, omitempty"`
	Message       string `json:"message"`
	SwaggeruiPath string `json:"swaggerui_path"`
}

func Set(r *httprouter.Router, swaggeruiPath string) {
	swaggerJson.SwaggeruiPath = "./swaggerui"
	if swaggeruiPath != "" {
		swaggerJson.SwaggeruiPath = swaggeruiPath
	}
	// Check Swagger file
	if err := checkSwaggerFile(swaggerJson.SwaggeruiPath); err != nil {
		swaggerJson.Status = strconv.Itoa(int(http.StatusInternalServerError))
		swaggerJson.Message = err.Error()
		r.GET("/swagger/*filepath", handlerFunc(swaggerError))
	} else {
		// Load Swagger UI
		dir, _ := os.Getwd()
		swaggerui := path.Join(dir, swaggerJson.SwaggeruiPath)
		r.ServeFiles("/swagger/*filepath", http.Dir(swaggerui))
		log.Debug("Swagger Started...")
	}
}

func checkSwaggerFile(swaggeruiPath string) error {
	if _, err := ioutil.ReadFile(swaggeruiPath + "/swagger.json"); err != nil {
		log.Error("SYSTEM", err, "error while loading swagger file swagger.json", log.Fields{"swagger_file_path": swaggeruiPath})
		return err
	}
	if _, err := ioutil.ReadFile(swaggeruiPath + "/index.html"); err != nil {
		log.Error("SYSTEM", err, "error while loading swagger file index.html", log.Fields{"swagger_file_path": swaggeruiPath})
		return err
	}
	return nil
}

func swaggerError(w http.ResponseWriter, r *http.Request) {
	js, _ := json.Marshal(swaggerJson)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(int(http.StatusInternalServerError))
	w.Write(js)
	return
}

func handlerFunc(h http.HandlerFunc) httprouter.Handle {
	return func(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
		context.Set(r, "params", p)
		h.ServeHTTP(w, r)
	}
}
