###### Swagger Website: https://goswagger.io/use/spec.html

## Setup Swagger:
```
docker pull quay.io/goswagger/swagger
docker run --rm -it -e GOPATH=$GOPATH -v $HOME:$HOME -w $(pwd) quay.io/goswagger/swagger version
```
## Setup Swagger Folder: 
######(default swaggerui path "./swaggerui")
Please copy `swaggerui` folder from this package into your Golang project root.

## Build Swagger: 
######(default json path "./swaggerui/swagger.json", json file must be inside your swaggerui folder)
```
docker run --rm -it -e GOPATH=$GOPATH -v $HOME:$HOME -w $(pwd) quay.io/goswagger/swagger generate spec -o ./swaggerui/swagger.json --scan-models
```