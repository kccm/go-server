package info

import (
	"encoding/json"
	"io/ioutil"
	"net/http"

	"github.com/gorilla/context"
	"github.com/julienschmidt/httprouter"

	log "gitlab.com/kccm/go-logger"
)

var (
	infoString applicationInfo
)

type applicationInfo struct {
	Version string `json:"version"`
	Short   string `json:"short"`
	Status  string `json:"status"`
	Build   string `json:"build"`
}

func Set(r *httprouter.Router, verFilePath string) {
	loadInfoString(verFilePath)
	r.GET("/info", handlerFunc(info))
}

// loadInfoString is meant to be called by a server during bootstrapping in
// order to cache the server software build's information string for use by te
// /info endpoint for version checking and liveness checking
func loadInfoString(verFilePath string) {
	data, err := ioutil.ReadFile(verFilePath) // "data/info.json"
	if err != nil {
		log.Fatal("SYSTEM", err, "error while opening version file", log.Fields{"version_file_path": verFilePath})
	}

	err = json.Unmarshal(data, &infoString)
	if err != nil {
		log.Fatal("SYSTEM", err, "error while unmarshal version file", log.Fields{"version_file_path": verFilePath})
	}
}

// swagger:operation GET /info Monitoring info
// Return server version infomation.
// This endpoint is also used for monitoring the server health.
// ---
// responses:
//   '200':
func info(w http.ResponseWriter, r *http.Request) {
	js, err := json.Marshal(infoString)
	if err != nil {
		http.Error(w, "JSON Error: "+err.Error(), http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(int(http.StatusOK))
	w.Write(js)
	return
}

func handlerFunc(h http.HandlerFunc) httprouter.Handle {
	return func(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
		context.Set(r, "params", p)
		h.ServeHTTP(w, r)
	}
}
